# coding=utf8
import json
import time

import requests as http
from bs4 import BeautifulSoup
from bson.objectid import ObjectId
from pymongo import MongoClient
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

SHOW_SELENIUM_BROWSER = False
ANTIGATE_KEY = ""


class Antigate():
    def __init__(self, print_log, key, sleep_time=5):
        self.print_log = print_log
        self.key = key
        self.sleep_time = sleep_time

    def _unlock_element(self, elem, driver):
        unlock_script = 'arguments[0].style.display = "block"; arguments[0].style.visibility = "visible"; arguments[0].style.height = "20px"'
        driver.execute_script(unlock_script, elem)

    def process_recaptcha(self, url, driver):
        token = driver.find_element_by_css_selector('.g-recaptcha').get_attribute('data-sitekey')
        ccode = self._recaptcha(site_key=token, site_url=url, driver=driver)
        if not ccode:
            raise Exception("Can not resolve ReCaptcha")
        if ccode == "solved by user":
            self.print_log("Капча решена пользователем")
            return
        c_input = driver.find_element_by_css_selector('#g-recaptcha-response')
        self._unlock_element(c_input, driver)
        c_input.send_keys(ccode)
        driver.find_element_by_name('submit').click()

    def _recaptcha(self, site_key, site_url, driver):
        data = {
            "clientKey": self.key,
            "task": {
                "type": "NoCaptchaTaskProxyless",
                "websiteURL": site_url,
                "websiteKey": site_key
            }
        }
        i = 0
        while True:
            if 'ipv4.google.com' not in driver.current_url:
                return 'solved by user'
            print("Start captcha resolving...")
            self.print_log(u'Попытка решить reCaptcha...')
            while True:
                if 'ipv4.google.com' not in driver.current_url:
                    return 'solved by user'
                try:
                    r = http.post("http://api.anti-captcha.com/createTask", data=json.dumps(data)).json()
                    if r.get("taskId"):
                        break
                    if r['errorCode'] == 'ERROR_NO_SLOT_AVAILABLE':
                        i += 1
                        print("ERROR_NO_SLOT_AVAILABLE on Antigate, waiting 1 sec... (Attempt %s)" % str(i))
                        self.print_log("ERROR_NO_SLOT_AVAILABLE on Antigate, waiting 1 sec... (Attempt %s)" % str(i))
                        time.sleep(1)
                    else:
                        break
                except:
                    pass

            if not r.get("errorId"):
                print("TaskId = %s" % r.get("taskId"))
                self.print_log("TaskId = %s" % r.get("taskId"))
                while True:
                    if 'ipv4.google.com' not in driver.current_url:
                        return 'solved by user'
                    time.sleep(self.sleep_time)
                    data = {
                        "clientKey": self.key,
                        "taskId": r.get("taskId")
                    }
                    print("Ack")
                    self.print_log(u'Проверка готовности ответа')
                    resp = http.post("http://api.anti-captcha.com/getTaskResult", data=json.dumps(data),
                                     timeout=15).json()
                    print(resp)
                    self.print_log(resp)
                    print(resp.get("status"))
                    self.print_log(resp.get("status"))
                    if resp.get("status") == "ready":
                        print("Captcha resolved")
                        self.print_log(u'reCaptcha пройдена')
                        return (resp.get("solution") or {}).get("gRecaptchaResponse")
                    if resp.get("errorId"):
                        return False
            else:
                print(r['errorCode'])
                self.print_log(r['errorCode'])
                print("Can not resolve captcha. Trying again...")
                self.print_log("Не удается решить reCaptcha, пробуем снова...")


class Selenium(object):
    def __init__(self, print_log, driver=None, user_agent=None, proxy_list=None):
        self.print_log = print_log
        self.proxy = proxy_list
        self.current_proxy = ''

        if not SHOW_SELENIUM_BROWSER:
            display = Display(visible=0, size=(800, 600))
            display.start()
        if driver:
            self.driver = driver
        elif not proxy_list and not user_agent:
            self.driver = webdriver.Firefox()
        else:
            profile = webdriver.FirefoxProfile()
            if user_agent:
                profile.set_preference("general.useragent.override", user_agent)
            self.driver = webdriver.Firefox(profile)

    def close(self):
        self.driver.close()

    def get_url(self, url, try_count=3, timeout=30, sleep_seconds=5):
        self.driver.set_page_load_timeout(timeout)
        result = ''
        for x in range(try_count):
            try:
                self.driver.get(url)
                time.sleep(1)
                result = self.driver.page_source
            except:
                try:
                    result = self.driver.page_source
                except:
                    pass

            if result and 'checking your browser' not in result.lower():
                return result
            else:
                time.sleep(sleep_seconds)
        return result


class GoogleSearch(object):
    def __init__(self, print_log, antigate_key, selenium, base, try_count=3, interval=1):
        self.print_log = print_log
        self.selenium = selenium
        self.antigate = Antigate(print_log, antigate_key)
        self.try_count = try_count
        self.interval = interval
        self.base = base
        self.template = "https://patents.google.com/?inventor=%s&page=%s" if base == "patent" else "https://scholar.google.ru/scholar?hl=en&as_sdt=0,5&q=%s&start=%s"

    def check_recaptcha(self):
        time.sleep(self.interval)
        for x in range(self.try_count):
            if 'ipv4.google.com' in self.selenium.driver.current_url:
                self.antigate.process_recaptcha(self.selenium.driver.current_url, self.selenium.driver)
            if 'ipv4.google.com' not in self.selenium.driver.current_url:
                return True

    def google_search_correctly(self, request, pages_count=1):
        for x in range(self.try_count):
            try:
                result = []
                url = self._make_url(request, 1)
                self.selenium.get_url(url)
                for page_num in range(pages_count):
                    if self.check_recaptcha():
                        url = self._make_url(request, page_num)
                        self.selenium.get_url(url)
                        if self.check_recaptcha():
                            result.append(self.selenium.driver.page_source)
                            time.sleep(self.interval)
                            # for page_num in range(pages_count - 1):
                            #     try:
                            #         if self.base == "scholar":
                            #             self.selenium.driver.find_element_by_css_selector(
                            #                 '#gs_n > center > table > tbody > tr > td:nth-child(12) > a > b').click()
                            #         else:
                            #             try:
                            #                 self.selenium.driver.find_element_by_css_selector(
                            #                     '#pnnext > span:nth-child(2)').click()
                            #             except:
                            #                 self.selenium.driver.find_element_by_id('pnnext').click()
                            #         time.sleep(self.interval)
                            #     except:
                            #         print("Maximum page %s reached" % str(page_num + 1))
                            #         self.print_log("Достигнута максимальная страница %s" % str(page_num + 1))
                            #         return result
                            # if self.check_recaptcha():
                            #     result.append(self.selenium.driver.page_source)
                return result
            except Exception as e:
                print('error while search: %s' % str(e))
                self.print_log('При поиске возникла ошибка: %s' % str(e))
        return ['']

    def google_search_hard(self, request, pages_count=1):
        result = []
        for page_num in range(pages_count):
            url = self._make_url(request, page_num)
            result.append(self._google_search(url))
        return result

    def _make_url(self, request, page):
        if self.base == 'scholar':
            return self.template % (request, 10 * page)
        else:
            return self.template % (request, page)

    def _google_search(self, url):
        for x in range(self.try_count):
            try:
                self.selenium.get_url(url)
                self.check_recaptcha()
                return self.selenium.driver.page_source
            except Exception as e:
                print(str(e))
                self.print_log(str(e))
        return ''

    def parse(self, html):
        result = []
        if html:
            try:
                soup = BeautifulSoup(html, 'lxml')
                all_links = soup.findAll('div', {'class': 'gs_ri'})
                for lnk in all_links:
                    lnk_date = None
                    try:
                        lnk_date = lnk.findAll('div', {'class': 'gs_a'})[0].text
                    except:
                        pass
                    result.append(
                        {'name': lnk.text, 'url': all_links[1].findAll('a')[2].attrs['href'], 'info': lnk_date})
            except Exception as e:
                print("Can not parse google results: %s" % str(e))
                self.print_log("Не удается прочитать результаты поиска: %s" % str(e))
        return result

    def parse_patent(self, html):
        result = []
        if html:
            try:
                soup = BeautifulSoup(html, 'lxml')
                all_links = soup.find('section').findAll('article')
                for lnk in all_links:
                    data = lnk.findAll('span', id='htmlContent')
                    try:
                        result.append(
                            {'name': data[0].text, 'inventor': data[2].text,
                             'url': lnk.find('state-modifier').attrs['data-result'],
                             'date': lnk.findAll('h4')[1].text})
                    except:
                        pass
            except Exception as e:
                print("Can not parse google results: %s" % str(e))
                self.print_log("Не удается прочитать результаты поиска: %s" % str(e))
        return result

    def search_and_parse(self, request, pages_count, method="google_search_correctly"):
        for x in range(self.try_count):
            try:
                htmls = getattr(self, method)(request, pages_count)
                parsed = []
                for html in htmls:
                    try:
                        if self.base == "patent":
                            parsed.extend(self.parse_patent(html))
                        else:
                            parsed.extend(self.parse(html))
                    except Exception as e:
                        print(str(e))
                return parsed
            except Exception as e:
                print(str(e))
                self.print_log(str(e))
        return [{}]


class ElibrarySearch(object):
    def __init__(self, print_log, selenium, try_count=10):
        self.print_log = print_log
        self.selenium = selenium
        self.try_count = try_count

    def search(self, request, pages_count=1):
        for x in range(self.try_count):
            try:
                result = []
                self.selenium.get_url("https://elibrary.ru/authors.asp")
                self.selenium.driver.find_element_by_name('surname').send_keys(request)
                time.sleep(2)
                self.selenium.driver.find_element_by_name('surname').send_keys(Keys.RETURN)
                time.sleep(2)
                result.append(self.selenium.driver.page_source)
                for page_num in range(pages_count - 1):
                    try:
                        self.selenium.driver.find_element_by_css_selector(
                            "#pages > table > tbody > tr > td:nth-child(13) > a").click()
                    except:
                        try:
                            self.selenium.driver.find_element_by_css_selector(
                                "#pages > table > tbody > tr > td:nth-child(8) > a").click()
                        except:
                            print("Maximum page %s reached" % str(page_num + 1))
                            self.print_log("Достигнута максимальная страница %s" % str(page_num + 1))
                            return result
                    result.append(self.selenium.driver.page_source)
                return result
            except Exception as e:
                print('error while search: %s' % str(e))
                self.print_log('При поиске возникла ошибка: %s' % str(e))
        return ['']

    def get_advanced(self, url_part):
        if not url_part:
            return []
        try:
            self.selenium.get_url("https://elibrary.ru/" + url_part)
            time.sleep(2)
            html = self.selenium.driver.page_source
            result = []
            soup = BeautifulSoup(html, 'lxml')
            rows = soup.findAll('table', attrs={"style": "border-spacing: 0px 2px;"})[1].findAll('tr')
            for row in rows:
                cols = row.find_all('td')
                cols = [ele.text.strip() for ele in cols]
                tmp = [ele for ele in cols if ele]
                result.append(tmp)  # Get rid of empty values
            return [x for x in result[2:] if x]
        except:
            return []

    def get_publications(self, url_part):
        if not url_part:
            return []
        try:
            self.selenium.get_url("https://elibrary.ru/" + url_part)
            time.sleep(2)
            html = self.selenium.driver.page_source
            result = []
            soup = BeautifulSoup(html, 'lxml')
            rows = soup.findAll('table', id="restab")[0].findAll('tr')
            for row in rows:
                cols = row.find_all('td')
                try:
                    link = ' Ссылка на оригинал: https://elibrary.ru' + cols[1].find('a').attrs['href']
                except:
                    link = None
                cols = [ele.text.strip() + link if link else '' for ele in cols]
                tmp = [ele for ele in cols if ele]
                result.append(tmp)  # Get rid of empty values
            return [x[1] for x in result[2:] if x]
        except:
            return []

    def parse(self, html, pubs=False):
        result = []
        if html:
            try:
                soup = BeautifulSoup(html, 'lxml')
                rows = soup.find('table', id="restab").findAll('tr')
                for row in rows:
                    cols = row.find_all('td')
                    cols = [ele.text.strip() for ele in cols]
                    tmp = [ele for ele in cols if ele]
                    try:
                        url_publications = row.contents[7].findAll('a')[0].attrs['href']
                        url_part = row.contents[7].findAll('a')[1].attrs['href']
                        publications = self.get_publications(url_publications) if pubs else []
                        tmp.append(publications)
                        tmp.append(url_part)
                    except:
                        tmp.append([])
                        tmp.append(None)
                    result.append(tmp)  # Get rid of empty values
            except Exception as e:
                print("Can not parse google results: %s" % str(e))
                self.print_log("Не удается прочитать результаты поиска: %s" % str(e))
        return [x for x in result[2:] if x and x != [[], None]]

    def search_and_parse(self, request, pages_count, pubs=False):
        for x in range(self.try_count):
            try:
                htmls = self.search(request, pages_count)
                parsed = []
                for html in htmls:
                    try:
                        parsed.extend(self.parse(html, pubs))
                    except Exception as e:
                        print(str(e))
                return parsed
            except Exception as e:
                print(str(e))
                self.print_log(str(e))
        return [{}]


class DissercastSearch(object):
    def __init__(self, print_log, selenium, try_count=3):
        self.print_log = print_log
        self.selenium = selenium
        self.try_count = try_count

    def search(self, request, pages_count=1):
        for x in range(self.try_count):
            try:
                result = []
                self.selenium.get_url("http://www.dissercat.com/search?keys=%s" % request)
                result.append(self.selenium.driver.page_source)
                for page_num in range(pages_count - 1):
                    try:
                        self.selenium.driver.find_element_by_css_selector(
                            '#content-content > div > div > div > ul > li.pager-next > a').click()
                    except:
                        print("Maximum page %s reached" % str(page_num + 1))
                        self.print_log("Достигнута максимальная страница %s" % str(page_num + 1))
                        return result
                    result.append(self.selenium.driver.page_source)
                return result
            except Exception as e:
                print('error while search: %s' % str(e))
                self.print_log('При поиске возникла ошибка: %s' % str(e))
        return ['']

    def parse(self, html):
        result = []
        if html:
            try:
                soup = BeautifulSoup(html, 'lxml')
                all_names = soup.find('dl', {'class': 'search-results'}).findAll('div', {'class': 'title'})
                all_infos = soup.find('dl', {'class': 'search-results'}).findAll('div', {'class': 'search-item-info'})
                for name, info in zip(all_names, all_infos):
                    result.append({'url': name.find('a').attrs['href'], 'title': name.text, "info": info.text})
            except Exception as e:
                print("Can not parse google results: %s" % str(e))
                self.print_log("Не удается прочитать результаты поиска: %s" % str(e))
        return result

    def search_and_parse(self, request, pages_count):
        for x in range(self.try_count):
            try:
                htmls = self.search(request, pages_count)
                parsed = []
                for html in htmls:
                    try:
                        parsed.extend(self.parse(html))
                    except Exception as e:
                        print(str(e))
                return parsed
            except Exception as e:
                print(str(e))
                self.print_log(str(e))
        return [{}]


class Person(object):
    def __init__(self, max_pages=10):
        self.mongo = MongoClient("localhost")
        self.antigate = ANTIGATE_KEY
        self.max_pages_count = max_pages
        self.selenium = Selenium(print)

    def create_query(self, query):
        query_id = self.mongo.documents.persons.find_one({"query": query})
        if not query_id:
            return str(self.mongo.documents.persons.save({"query": query}))
        else:
            return str(query_id.get('_id'))

    def transform_query_to_result(self, query_id, full_name):
        self.mongo.documents.persons.update({"_id": ObjectId(query_id)}, {"$set": {
            "full_name": full_name,
            "query": "Поиск по запросу {}".format(query_id)}})
        return query_id

    def search(self, base, person_id, _selected_url):
        try:
            person = self.mongo.documents.persons.find_one({"_id": person_id})
            person['full_name'] = person['full_name'].replace('  ', ' ')
            if base == "scholar":
                try:
                    gs = GoogleSearch(print, self.antigate, self.selenium, "scholar", 3)
                    res = gs.search_and_parse(person["full_name"], self.max_pages_count)
                    articles = len([x for x in res if "CITATION" not in x["name"]])
                    citations = len([x for x in res if "CITATION" in x["name"]])
                    self.mongo.documents.persons.update({"_id": person_id}, {"$set": {
                        "scholar": {"art_count": articles,
                                    "citations": citations}}})
                except Exception as e:
                    print("schoolar error " + str(e))
                    self.mongo.documents.persons.update({"_id": person_id},
                                                        {"$set": {"scholar": {"art_count": "No data",
                                                                              "citations": "No data"}}})
            elif base == "patent":
                try:
                    gs = GoogleSearch(print, self.antigate, self.selenium, "patent", 3)
                    res = gs.search_and_parse(person["full_name"], self.max_pages_count)
                    dates = []
                    for r in res:
                        try:
                            _date = r["date"].split("Publication ")[1].split(" ")[0].strip()
                            dates.append(_date)
                        except:
                            pass
                    self.mongo.documents.persons.update({"_id": person_id}, {"$set": {
                        "patent": {"count": len(res),
                                   "dates": dates}}})
                except Exception as e:
                    print("Patents error " + str(e))
                    self.mongo.documents.persons.update({"_id": person_id},
                                                        {"$set": {"patent": {"count": "No data"}}})
            elif base == "elibrary":
                try:
                    es = ElibrarySearch(print, self.selenium)
                    res = es.search_and_parse(person["full_name"], self.max_pages_count, pubs=True)
                    suggestions = []
                    for x in res:
                        name = x[1].split('\n\n')[0]
                        try:
                            work = x[1].split('\n\n')[1]
                        except:
                            work = ''
                        suggestions.append({
                            "name": name,
                            "work": work,
                            "publications": x[2],
                            "citations": x[3],
                            "xirsh": x[4],
                            "last_articles": x[5],
                            "url_part": x[6]})
                    try:
                        selected = [y for y in suggestions if y['url_part'] == _selected_url][0]
                    except:
                        selected = suggestions[0]
                    advanced = es.get_advanced(selected["url_part"])
                    tmp = []
                    for x in advanced:
                        try:
                            tmp.append({"name": x[0], "value": x[1]})
                        except:
                            pass
                    self.mongo.documents.persons.update({"_id": person_id},
                                                        {"$set": {"hirsh": {"main": selected, "additional": tmp}}})
                except Exception as e:
                    print("Elibrary error " + str(e))
                    self.mongo.documents.persons.update({"_id": person_id},
                                                        {"$set": {"hirsh": {
                                                            "main": {"publications": "No data", "citations": "No data"},
                                                            "additional": "No data"}}})
            elif base == "dissertations":
                try:
                    ds = DissercastSearch(print, self.selenium)
                    res = ds.search_and_parse(person["full_name"], self.max_pages_count)
                    try:
                        last_year = sorted([int(x["info"][-4:]) for x in res])[-1]
                    except:
                        last_year = None
                    self.mongo.documents.persons.update({"_id": person_id}, {"$set": {
                        "dissercast": {"count": len(res),
                                       "dates": last_year}}})
                except Exception as e:
                    print("Dissercast error " + str(e))
                    self.mongo.documents.persons.update({"_id": person_id},
                                                        {"$set": {"dissercast": {"count": "No data"}}})
        except Exception as e:
            print("Ошибка %s" % str(e))

    def full_search(self, person_id, _selected_url):
        for sourse in ['elibrary', 'scholar', 'patent', 'dissertations']:
            print(sourse)
            try:
                self.search(sourse, ObjectId(person_id), _selected_url)
            except Exception as e:
                print(str(e))
            print(sourse)

    def return_dict(self, person_id):
        return self.mongo.documents.persons.find_one({"_id": ObjectId(person_id)})

    def get_suggestions(self, query):
        query_id = self.create_query(query=query)
        es = ElibrarySearch(print, self.selenium)
        res = es.search_and_parse(query, self.max_pages_count)
        suggestions = []
        j = 1
        for i, x in enumerate(res):
            name = x[1].split('\n\n')[0].replace('*', '') + "; id=" + str(x[6])
            try:
                work = x[1].split('\n\n')[1]
            except:
                work = ''
            last_articles = ';    '.join(x[5])
            last_articles = last_articles[:100] if len(last_articles) > 100 else last_articles
            suggestions.append({
                "name": name,
                "work": work,
                "publications": x[2],
                "citations": x[3],
                "xirsh": x[4],
                "last_articles": last_articles,
                "url_part": x[6],
                "number": j}
            )
            j += 1
        return suggestions, query_id
