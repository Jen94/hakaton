import os

from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

from classes.parsers import Person

APP_ROOT = os.path.dirname(os.path.abspath(__file__))
# UPLOAD_FOLD = '/Users/blabla/Desktop/kenetelli/htmlfi'
UPLOAD_FOLDER = os.path.join(APP_ROOT)
flask_thread = None
settings = {}
result = None
log_list = []  # [('category': 'msg')]
app = Flask(__name__)
# set the secret key.  keep this really secret:
app.secret_key = 'A0Zr98j/3yX R~XHH!jmN]LWX/,?RT'
files_path = '/home/user/NewsProject_files'
mongo = MongoClient("localhost")


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/select', methods=['GET'])
def person_select():
    """
    ip:port/select?person_search=IVAn
    :return:
    """
    query = request.args.get('person_search')
    if query:
        full_info = mongo.documents.persons.find_one({"full_name": query})
        if full_info:
            return render_template('perform.html', profile_dict=full_info)
        person = Person(max_pages=10)
        suggestions, query_id = person.get_suggestions(query)
        return render_template('select.html', suggestions=suggestions, query_id=query_id)
    else:
        return redirect(url_for('index'))


@app.route('/perform', methods=['GET', 'POST'])
def profile_performer():
    if request.method == 'POST':
        query_id = request.form.get('query_id', '')
        name = request.form.get('full_name', '')
    else:
        query_id = request.args.get('query_id')
        name = request.args.get('full_name')
    if not (query_id and name):
        return redirect(url_for('index'))

    _name = name.split(';')[0].replace('  ', ' ')
    try:
        _selected_url = name.split('; ')[1].replace('id=', '')
        _selected_url = None if _selected_url == 'None' else _selected_url
    except:
        _selected_url = 0
    person = Person(max_pages=10)
    profile_id = person.transform_query_to_result(query_id, _name)

    person.full_search(profile_id, _selected_url)
    full_info = person.return_dict(profile_id)

    # tmp = {}
    # for x in full_info['hirsh']['additional']:
    #     tmp[x['name']] = x['value']
    # full_info['hirsh']['detail'] = json.dumps(tmp, ensure_ascii=False, indent=4)

    return render_template('perform.html', profile_dict=full_info)


app.run(host='0.0.0.0', port=5000, debug=True)
